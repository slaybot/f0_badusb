#!/bin/bash

clear

# Function to draw the stickman
draw_stickman() {
    echo "  O"
    echo " /|\\"
    echo " / \\"
}

# Loop to animate the stickman
while true; do
    clear
    echo "  \\O/"
    echo "   |"
    echo "  / \\"
    sleep 0.2
    clear
    draw_stickman
    sleep 0.2
done
